'''
Created on Jan 29, 2016

@author: christos
'''

class binaryOptionsPrediction(object):
    '''
    classdocs
    '''
    
    
    __originalFinancialInstrument = []
    __sizeOforiginalFinancialInstrument = 0


    def __init__(self, originalFinancialInstrument):
        '''
        Constructor
        '''
        self.__originalFinancialInstrument = originalFinancialInstrument 
        self.__sizeOforiginalFinancialInstrument = len(self.__originalFinancialInstrument)
        
    
    def predictNDaysFromToday(self):
        pass
    
    # return the callTrend
    def evaluateSeries(self):
        callTrend = []
        for index in range(self.__sizeOforiginalFinancialInstrument-1):
            pairwiseComparison = False
            currentValue = self.__originalFinancialInstrument[index]
            nextValue = self.__originalFinancialInstrument[index+1]
            #todo maybe convert to pips first, before comparing
            if currentValue < nextValue:
                pairwiseComparison = True
                
            callTrend.append(pairwiseComparison)
        return callTrend
    
    
    def dummyPrediction(self):
        return 23.2
    
    def predictNextDay(self):
        return self.dummyPrediction();
    
    def createReport(self, finalPlotScriptFilename , timeSeriesFilename, positiveImpulsesFilename, nameOfInstrument, successRate ):
        with open(finalPlotScriptFilename, 'w') as plotScript:
            plotScript.write("reset"+"\n")
            plotScript.write("clear"+"\n")
            plotScript.write("set grid"+"\n")
            plotScript.write("set style data linespoints"+"\n")
            plotScript.write("set key below"+"\n")
            plotScript.write("set ylabel 'value of "+str(nameOfInstrument)+"'\n")
            plotScript.write("set xlabel 'success rate:"+str(successRate)+"%' \n")
            plotScript.write("plot '"+str(timeSeriesFilename)+"' using 1:2 title 'actual data', '"+str(positiveImpulsesFilename)+"' with impulses linewidth 5 title 'Call'"+"\n")
        print 'PlotScript: '+finalPlotScriptFilename+' was created successfully!'
            
            
    def exampleOfPlot(self):
        self.createReport('report2.gp', 'exampleTimeSeries.txt', 'examplePositiveImpulses.txt', 'imaginary pair', 81.2)
        
    

if __name__ == "__main__" :
    b1 = binaryOptionsPrediction([1])
    print "binaryOptionsPrediction"   
    b1.exampleOfPlot()
        