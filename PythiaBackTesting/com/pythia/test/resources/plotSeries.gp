reset
clear
set grid
set style data linespoints
set key below
plot 'exampleTimeSeries.txt' using 1:2 title 'actual data', 'examplePositiveImpulses.txt' with impulses linewidth 5 title 'Call'
set xlabel 'success x'
set ylabel 'value'
