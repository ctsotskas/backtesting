'''
Created on Jan 29, 2016

@author: christos
'''
import unittest

from ..src import binaryOptionsPrediction

class Test(unittest.TestCase):
    #an example time series
    __actualTimeSeries = [1.01, 1.04, 1.03, 1.05, 1.06, 1.3, 1.2, 1.11, 1.00]
    
    # true when it actually calls, false otherwise
    __actualCallTrend = [True, False, True, True, True, False, False, False]
    
    #true trend results
    __actualTrendResults = ["call", "put", "call", "call", "call", "put", "put", "put"]    

    __pythia = None
    
    #numerical tolerance when comparing numerical values
    __tolerance = 0.001
    
    __numberOfActualCalls = 4
    
    
    
    
    #ancilary method to numerically compare if two vectors are identical given certain accuracy
    def compareEqualityBetweenTwoVectors(self, vectorA, vectorB ):
        equality = True
        for index in range(len(vectorA)):
            if abs( vectorA[index] - vectorB[index]) > self.__tolerance :
                equality = False
        
        return equality
        
    
    def setUp(self):       
        self.__pythia = binaryOptionsPrediction.binaryOptionsPrediction(self.__actualTimeSeries)


    def tearDown(self):
        pass


    def testEvaluateASeries(self):
        calculatedCallTrend = self.__pythia.evaluateSeries()
        
        twoTrendsBeingEqual = self.compareEqualityBetweenTwoVectors(calculatedCallTrend, self.__actualCallTrend)
        self.assertTrue(twoTrendsBeingEqual, "The calculated callTrend does not match expected value")
        
    
    
    def testGenerateCallTrendFromResults(self):
        pass
    
    def testPredictionIsNotTheSameAsBefore(self):
        
        previousValue = self.__actualTimeSeries[-1]
        predictedValue = self.__pythia.predictNextDay()
        
        self.assertNotEqual(previousValue, predictedValue, "the predicted value is IDENTICAL to the previous one")
    
    
    def testNumberOfAcualCallsDetected(self):
        currentlyFoundCalls = 0
        for i in self.__actualCallTrend:
            if i is True:
                currentlyFoundCalls = currentlyFoundCalls + 1
                
        self.assertEqual(currentlyFoundCalls, self.__numberOfActualCalls, 'fewer of more Calls were found in the trend')
        
        
    def testCreatingAnAutomatedPlot(self):
        self.__pythia.createReport('resources/report2.gp', 'exampleTimeSeries.txt', 'examplePositiveImpulses.txt', 'imaginary pair', 81.2)
        
    

if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
